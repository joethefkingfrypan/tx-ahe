## TPE - Système de gestion des AHE

#### Technologies utilisées

- PHP avec framework Symfony2.
- JavaScript.
- HTML/CSS.

#### Configuration initiale de l'environnement de travail

1. Téléchargement et installation de `XAMPP v.5.5.19` sur le [site officiel](https://www.apachefriends.org/download.html).
2. Téléchargement et installation de `Git v.1.9.5` sur le [site officiel](http://git-scm.com/download/win).
3. [*Optionel mais recommandé*] Téléchargement et installation de `Github` sur le ite officiel pour [Windows](https://windows.github.com/) ou pour [Mac](https://mac.github.com/).
4. Ajouter `C:\Program Files (x86)\Git\bin;C:\xampp\mysql\bin;C:\xampp\php;C:\xampp\php\pear;` à la fin de la variable d'environnement __PATH__. _(Voir ci-dessous pour plus de détails)_.
5. Ouvrir un terminal en tant qu'administrateur.
6. Ouvrir le fichier `C:\xampp\php\php.ini` et activer l'extension `extension=php_intl.dll` en enlevant le ";" en début de ligne.

#### Détails sur la procédure de modification du PATH sous Windows

1. Sous Windows, ouvrir le `menu Démarrer`.
2. Faire clic-droit sur `Ordinateur`/`Ce PC`.
3. Sélectionner `Propriétés`.
4. Dans la barre de gauche, cliquer sur `Paramètres systèmes avancés`.
5. Cliquer sur le bouton `Variables d'environnement`.
6. Sélectionner `Path` dans la section `Variables systèmes`.
7. Cliquer sur `Modifier`.
8. Appliquer les modifications souhaitées. 
9. Valider tous les écrans en cliquant sur `OK`).

## Récupérer les dernières versions du projet

Si vous n'avez pas encore récupérer les sources du projet, veuillez vous référer à la section "__Cloner le projet__", dans le cas contraire passer directement à "__Mettre à jour les sources__"

#### Cloner le projet

Le projet est actuellement disponible sur un système de gestion de révisions Git hébergé sur BitBucket. La commande pour cloner le dépôt est la suivante : `git clone git@bitbucket.org:joethefkingfrypan/tx-ahe.git`

Une fois fait, se rendre dans le dossier nouvellement créé, puis dans le dossier `Sources`. Là, exécuter `php -r "readfile('https://getcomposer.org/installer');" | php`

Vous devriez alors avoir l'aborescence suivante :

    └── tx-ahe
        └── References : contient les documents externes.
        └── Sources : contient toutes les sources du projet.
            └── composer.phar
            └── app
            └── src
            └── ...

#### Mettre à jour les sources

1. Ouvrir un terminal (`git shell` ou `powershell`)
2. Se rendre dans le dossier du dépôt git (`tx_ahe`)
3. Exécuter la commande `git pull`
4. S'il n'y a pas de message particulier à l'étape précédente, se rendre dans le dossier `Sources` du dépôt git

__NOTE__ : 

- Si le fichier `composer.phar` n'est pas présent le dossier `Sources`, se référer à la section précédente.
- Les étapes suivantes doivent être réalisées avec un terminal avec les droits administrateurs (*pour pouvoir créer des liens symboliques*). En effet `MopaBootstrapBundle` utilise un script post-installation pour créer un lien symbolique vers les sources de `Bootstrap Twitter`. Si la console n'a pas les droits administrateurs, la commande suivante (php composer.phar update) générera un message d'erreur.

1. Exécuter la commande `php composer.phar update`
2. Exécuter la commande `php app/console assets:install`
3. Exécuter la commande `php app/console assetic:dump`
4. Enfin pour lancer le serveur, exécuter la commande `php app/console server:run` (conserver la console ouverte)
5. Le serveur est désormais accessible à l'adresse : http://localhost:8000

##### Si impossibilité d'avoir un terminal administrateur

Dans le cas où il vous est impossible d'avoir un accès administrateur, il vous suffit de vous rendre dans :
    
    └── tx-ahe
        └── Sources
            └── vendor
                └── twbs

Copier le dossier nommé `bootstrap` et le coller dans :

    └── tx-ahe
        └── Sources
            └── vendor
                └── mopa
                    └── bootstrap-bundle
                        └── Mopa
                            └── Bundle
                                └── BootstrapBundle
                                    └── Resources
                                        └── public

Vous obtenez ainsi l'aborescence suivante :

    └── tx-ahe
        └── Sources
            └── vendor
                └── twbs
                    └── bootstrap
                └── mopa
                    └── bootstrap-bundle
                        └── Mopa
                            └── Bundle
                                └── BootstrapBundle
                                    └── Resources
                                        └── public
                                            └── bootstrap

Ensuite, éditer le fichier `Sources/composer.json` et supprimer les 2 occurences de la ligne suivante (Il y en a une dans la partie `post-install-cmd` et une autre dans `post-update-cmd`)
```json
 "Mopa\\Bundle\\BootstrapBundle\\Composer\\ScriptHandler::postInstallSymlinkTwitterBootstrap"
```


## Informations

#### Emplacements et fichiers importants

__Emplacements de base__:

- Fichier définissant les paramètres de sécurité : `app/config/security.yml`
- Fichier de configuration principal : `app/config/parameters.yml`
- Fichier de routing principal : `app/config/routing.yml`
- Fichier de routing pour l'environnement de développement : `app/config/routing_dev.yml`
- Fichier contenant les paramètres (_notamment ceux d'accès de BDD_) : `app/config/parameters.yml`
- Fichier définissant les assets utilisés : `app/config/assets.yml`
- Vues communes à tous les bundles : `app/resources/views`

__Emplacement liés au bundle principal__ :

- Fichier de routing : `src/TxAhe/MainBundle/Resources/config/routing.yml`
- Dossier contenant les vues `src/TxAhe/MainBundle/Resources/Views`

#### Liste des URL disponibles

__NOTE__: Ce README n'étant pas modifié dynamiquement, les routes présentes ci-dessous peuvent avoir changé. Les fichiers fichiers ,  et  contiennent tous deux l'ensemble des routes de l'application.

- http://localhost:8000/login
- http://localhost:8000/profil
- http://localhost:8000/ahe/creer
- http://localhost:8000/ahe/liste
- http://localhost:8000/ahe/validation
- http://localhost:8000/ahe/details/__ID__ 

Routes valables uniquement dans l'environnement de développement :

- http://localhost:8000/_createDbUsers
- http://localhost:8000/_createDbCategories
- http://localhost:8000/_clearDb

#### Documentation subsidiaire

- Documentation officielle : http://symfony.com/doc
- Guide PDF contenant pleins d'infos et d'exemples : [lien de téléchargement](http://symfony.com/pdf/Symfony_book_2.6.pdf?v=4)

## Optionnel - Installer un accélérateur PHP

L'accélérateur APC ne vient pas nativement avec PHP, il faut donc télécharger à part la DLL qui correspond à la bonne version de PHP. 

##### Téléchargement de l'extension PHP

Si vous avez installé `XAMPP v.5.5.19` alors vous pouvez immédiatement télécharger la version [4.0.7 Thead Safe (SF) x86](http://windows.php.net/downloads/pecl/releases/apcu/4.0.7/php_apcu-4.0.7-5.5-ts-vc11-x86.zip) ou [4.0.7 TheadSafe (SF) x64](http://windows.php.net/downloads/pecl/releases/apcu/4.0.7/php_apcu-4.0.7-5.5-ts-vc11-x64.zip)

Sinon, rendez-vous sur [cette page](http://pecl.php.net/package/APCu/4.0.7/windows) et télécharger la version qui correspond à votre version de PHP.

##### Installation et activation de l'extension

1. Ouvrez l'archive que vous venez de télécharger.
2. Rendez-vous dans `C:\xampp\php\ext`
3. Extrayer le fichier `php_apcu.dll` dans ce dossier.
4. Revenez dans le dossier parent _(C:\xampp\php)_ et ouvrez le fichier `php.ini`
5. Descendez jusqu'à la section `; Dynamic Extensions ;`
6. Insérer dans cette section la ligne suivante : `extension=php_apcu.dll`
7. Sauvegarder le fichier.
8. Redemarrer le serveur Apache ou l'application Symfony.

## Pour aller plus loin

#### Créer un nouveau projet Symfony (from scratch)

Pour créer un nouveau projet, il suffit de procéder de la manière suivante : 

1. Via ligne de commande se rendre dans l'espace de travail souhaité.
2. Télécharger l'installateur de Symfony en exécutant la commande `php -r "readfile('http://symfony.com/installer');" > symfony.phar`
3. Exécuter la commande `symfony.phar self-update`
4. Exécuter `php symfony.phar new <PROJECT_NAME>`
5. Se rendre dans le dossier généré.
6. Exécuter `php app/console assets:install web` afin de générer toutes les ressources de base _(CSS/JS/etc)_.
7. Exécuter `php -r "readfile('https://getcomposer.org/installer');" | php` pour récupérer le fichier `composer.phar` qui sera utilisé pour le téléchargement de paquets d'extensions.
8. Exécuter `php composer.phar install` pour l'installer complètement.
9. Exécuter `php app/console server:run` afin de lancer le serveur _(Nul besoin d'avoir Apache lancé/allumé !)_.
10. Laisser cette console ouverte et se rendre à l'URL http://localhost:8000/config pour la configuration intiale.

![setup.jpg](https://bitbucket.org/repo/ejzk8M/images/2103204758-setup.jpg)
