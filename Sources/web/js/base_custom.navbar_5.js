/*jslint browser: true*/
/*global $, jQuery, alert, Routing, console*/

$(document).ready(function () {
    "use strict";

    /*+=================================+*
     *|            FUNCTIONS            |*
     *+=================================+*/

    function isToggled(elem) {
        return "true" === elem.attr('aria-expanded');
    }

    function isNotToggled(elem) {
        return "false" === elem.attr('aria-expanded');
    }

    function hideSubmenu(elem) {
        if(isToggled(elem)) {
            elem.collapse('toggle');
        }
    }

    function toggleOpacity(elem) {
        if(elem.css('opacity') == 0) {
            elem.fadeTo("250",1);
        } else {
            elem.fadeTo("250",0);
        }
    }

    function toggleVisibility(submenu, wrapper) {
        hideSubmenu(submenu);
        toggleOpacity(wrapper);
    }

    /*+=================================+*
     *|             PROCESS             |*
     *+=================================+*/

    // Navbar buttons
    var buttonAHE = $('#button-nav-collapse2');
    var buttonProfil = $('#button-nav-collapse3');

    // Navbar submenus
    var navAdministration = $('#nav-collapse1');
    var navAHE = $('#nav-collapse2');

    // Navbar submenu wrappers
    var navWrapperAHE = $('#navAhe');
    var navWrapperAdministration = $('#navAdministration');

    buttonAHE.click(function(){
        toggleVisibility(navAdministration, navWrapperAdministration);
    });

    buttonProfil.click(function(){
        if(isNotToggled(buttonAHE)) {
            // Do not toggle visibility if Administration button
            // has already been hidden by pressing AHE button
            toggleVisibility(navAdministration, navWrapperAdministration);
        }
        toggleVisibility(navAHE, navWrapperAHE);
    });
});