<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_encadrement")
 */
class AheDemandeEncadrement {

    function __construct($encadremmentPrevisionnel, $volumeHoraireEncadrement, $budgetEncadrement) {
        $this->encadremmentPrevisionnel = $encadremmentPrevisionnel;
        $this->volumeHoraireEncadrement = $volumeHoraireEncadrement;
        $this->budgetEncadrement = $budgetEncadrement;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $encadremmentPrevisionnel;

    /**
     * @ORM\Column(type="string")
     */
    protected $volumeHoraireEncadrement;

    /**
     * @ORM\Column(type="text")
     */
    protected $budgetEncadrement;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEncadremmentPrevisionnel() {
        return $this->encadremmentPrevisionnel;
    }

    /**
     * @param string $encadremmentPrevisionnel
     */
    public function setEncadremmentPrevisionnel($encadremmentPrevisionnel) {
        $this->encadremmentPrevisionnel = $encadremmentPrevisionnel;
    }

    /**
     * @return string
     */
    public function getVolumeHoraireEncadrement() {
        return $this->volumeHoraireEncadrement;
    }

    /**
     * @param string $volumeHoraireEncadrement
     */
    public function setVolumeHoraireEncadrement($volumeHoraireEncadrement) {
        $this->volumeHoraireEncadrement = $volumeHoraireEncadrement;
    }

    /**
     * @return string
     */
    public function getBudgetEncadrement() {
        return $this->budgetEncadrement;
    }

    /**
     * @param string $budgetEncadrement
     */
    public function setBudgetEncadrement($budgetEncadrement) {
        $this->budgetEncadrement = $budgetEncadrement;
    }
}