<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_materiel")
 */
class AheDemandeMateriel {

    function __construct($materielPrevisionnel) {
        $this->materielPrevisionnel = $materielPrevisionnel;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $materielPrevisionnel;


    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */


    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMaterielPrevisionnel() {
        return $this->materielPrevisionnel;
    }

    /**
     * @param string $materielPrevisionnel
     */
    public function setMaterielPrevisionnel($materielPrevisionnel) {
        $this->materielPrevisionnel = $materielPrevisionnel;
    }
}
