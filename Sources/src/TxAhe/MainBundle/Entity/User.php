<?php

namespace TxAhe\MainBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser {

    public function __construct($username,$email,$plainPass,$roles,$statut,$prenom,$nom,$cursus) {
        parent::__construct();
        $this->setUsername($username);
        $this->setEmail($email);
        $this->setPlainPassword($plainPass);
        $this->setRoles($roles);
        $this->setStatut($statut);
        $this->setPrenom($prenom);
        $this->setNom($nom);
        $this->setCursus($cursus);
        $this->setEnabled(true);
    }

    /* +---------------------------------------------+ *
     * |                INFOS DE BASE                | *
     * +---------------------------------------------+ */

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @ORM\Column(type="string") **/
    protected $nom;
    /** @ORM\Column(type="string") **/
    protected $prenom;
    /** @ORM\Column(type="string") **/
    protected $statut;
    /** @ORM\Column(type="string") **/
    protected $cursus;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom) {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut) {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getStatut() {
        return $this->statut;
    }

    /**
     * @return string
     */
    public function getCursus() {
        return $this->cursus;
    }

    /**
     * @param string $cursus
     */
    public function setCursus($cursus) {
        $this->cursus = $cursus;
    }
}