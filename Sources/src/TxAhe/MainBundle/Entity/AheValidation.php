<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_validation")
 */
class AheValidation {

    function __construct($statut,$commentaireValidation,$dateValidation) {
        $this->setStatut($statut);
        $this->setCommentaireValidation($commentaireValidation);
        $this->setDateValidation($dateValidation);
    }

    /* +---------------------------------------------+ *
     * |                INFOS DE BASE                | *
     * +---------------------------------------------+ */

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateValidation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $statut;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $commentaireValidation;


    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */


    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidation() {
        return $this->dateValidation;
    }

    /**
     * @param \DateTime $dateValidation
     */
    public function setDateValidation($dateValidation) {
        $this->dateValidation = $dateValidation;
    }

    /**
     * @return string
     */
    public function getStatut() {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut) {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getCommentaireValidation() {
        return $this->commentaireValidation;
    }

    /**
     * @param string $livrablePrevisionnel
     */
    public function setCommentaireValidation($commentaireValidation) {
        $this->commentaireValidation = $commentaireValidation;
    }
}
