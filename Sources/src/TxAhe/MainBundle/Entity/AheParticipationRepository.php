<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AheParticipationRepository extends EntityRepository {
    public function findUserApplicationStatus($userId,$aheId) {
        $dql = "SELECT participation.statut FROM TxAheMainBundle:AheParticipation participation WHERE participation.etudiant=$userId AND participation.ahe=$aheId";
        $query = $this->getEntityManager()->createQuery($dql)->setMaxResults(1);
        return $query->getResult();
    }

    public function getNumberOfParticipants($aheId) {
        $dql = "SELECT COUNT(participation.etudiant) as participants FROM TxAheMainBundle:AheParticipation participation WHERE participation.ahe=$aheId AND (participation.statut LIKE '%Acceptée' OR participation.statut LIKE '%Terminée')";
        $query = $this->getEntityManager()->createQuery($dql)->setMaxResults(1);
        return $query->getSingleResult();
    }

    public function getApplicationStatus($userId,$aheId) {
        $participation = $this->findUserApplicationStatus($userId,$aheId);
        $results = null;

        if(empty($participation)) {
            $results = array (
                "possible" => true
            );
        } else {
            $results = array (
                "possible" => false,
                "statut" => $participation[0]["statut"]
            );
        }

        return array_merge($results,$this->getNumberOfParticipants($aheId));
    }
}