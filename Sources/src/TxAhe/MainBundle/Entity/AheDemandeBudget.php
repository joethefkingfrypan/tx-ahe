<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_budget")
 */
class AheDemandeBudget {

    function __construct($budgetPrevisionnel, $detailsBudget) {
        $this->budgetPrevisionnel = $budgetPrevisionnel;
        $this->detailsBudget = $detailsBudget;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $budgetPrevisionnel;

    /**
     * @ORM\Column(type="text")
     */
    protected $detailsBudget;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getBudgetPrevisionnel() {
        return $this->budgetPrevisionnel;
    }

    /**
     * @param integer $budgetPrevisionnel
     */
    public function setBudgetPrevisionnel($budgetPrevisionnel) {
        $this->budgetPrevisionnel = $budgetPrevisionnel;
    }

    /**
     * @return string
     */
    public function getDetailsBudget() {
        return $this->detailsBudget;
    }

    /**
     * @param string $detailsBudget
     */
    public function setDetailsBudget($detailsBudget) {
        $this->detailsBudget = $detailsBudget;
    }
}