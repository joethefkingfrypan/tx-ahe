<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_categories")
 */
class AheCategorie {

    function __construct($categorie) {
        $this->categorie = $categorie;
    }

    /* +---------------------------------------------+ *
     * |                INFOS DE BASE                | *
     * +---------------------------------------------+ */

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @ORM\Column(type="string", unique=true, nullable=false) **/
    protected $categorie;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategorie() {
        return $this->categorie;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }
}