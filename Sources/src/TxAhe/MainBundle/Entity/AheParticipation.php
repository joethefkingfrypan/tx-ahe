<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TxAhe\MainBundle\Entity\AheParticipationRepository")
 * @ORM\Table(name="ahe_participation")
 */
class AheParticipation {

    function __construct($ahe, $etudiant, $commentaireParticipation) {
        $this->ahe = $ahe;
        $this->etudiant = $etudiant;
        $this->statut = "En attente";
        $this->commentaireParticipation = $commentaireParticipation;
    }

    /* +---------------------------------------------+ *
     * |                INFOS DE BASE                | *
     * +---------------------------------------------+ */

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Ahe")
     * @ORM\JoinColumn(name="ahe", referencedColumnName="id")
     */
    protected $ahe;

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="etudiant", referencedColumnName="id")
     */
    protected $etudiant;

    /**
     * @ORM\Column(type="string")
     */
    protected $statut;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $resultat;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $commentaireParticipation;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return mixed
     */
    public function getAhe() {
        return $this->ahe;
    }

    /**
     * @param mixed $ahe
     */
    public function setAhe($ahe) {
        $this->ahe = $ahe;
    }

    /**
     * @return mixed
     */
    public function getEtudiant() {
        return $this->etudiant;
    }

    /**
     * @param mixed $etudiant
     */
    public function setEtudiant($etudiant) {
        $this->etudiant = $etudiant;
    }

    /**
     * @return string
     */
    public function getStatut() {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut) {
        $this->statut = $statut;
    }

    /**
     * @return integer
     */
    public function getResultat() {
        return $this->resultat;
    }

    /**
     * @param integer $resultat
     */
    public function setResultat($resultat) {
        $this->resultat = $resultat;
    }

    /**
     * @return string
     */
    public function getCommentaireParticipation() {
        return $this->commentaireParticipation;
    }

    /**
     * @param string $commentaireParticipation
     */
    public function setCommentaireParticipation($commentaireParticipation) {
        $this->commentaireParticipation = $commentaireParticipation;
    }


}
