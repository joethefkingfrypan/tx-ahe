<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AheRepository extends EntityRepository {
    public function getCount() {
        $query = $this->createQueryBuilder('ahe')
            ->select('COUNT(ahe)')
            ->getQuery()
            ->getSingleScalarResult();
        return $query->getResult();
    }

    public function findAheCreatedByUser($userId) {
        $query = $this->createQueryBuilder("ahe")
            ->select('ahe')
            ->where("ahe.encadrant = :encadrant")
            ->setParameter("encadrant",$userId)
            ->getQuery();
        return $query->getResult();
    }
}
