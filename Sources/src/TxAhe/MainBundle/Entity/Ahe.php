<?php

namespace TxAhe\MainBundle\Entity;

use \Datetime;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TxAhe\MainBundle\Entity\AheRepository")
 * @ORM\Table(name="ahe")
 */
class Ahe {

    /* +---------------------------------------------+ *
     * |                INFOS DE BASE                | *
     * +---------------------------------------------+ */

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @ORM\Column(type="string") **/
    protected $intitule;
    /** @ORM\Column(type="text") **/
    protected $description;
    /** @ORM\Column(type="string") **/
    protected $categorie;
    /** @ORM\Column(type="string") **/
    protected $typeActivite;
    /** @ORM\Column(type="string") **/
    protected $volumeHoraire;
    /** @ORM\Column(type="integer") **/
    protected $nombreMaxParticipants;
    /** @ORM\Column(type="datetime") **/
    protected $dateDebut;
    /** @ORM\Column(type="datetime") **/
    protected $dateFin;
    /** @ORM\Column(type="text") **/
    protected $interetPedagogique;
    /** @ORM\Column(type="text") **/
    protected $resultatsAttendus;


    /* +---------------------------------------------+ *
     * |                  RELATIONS                  | *
     * +---------------------------------------------+ */

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="encadrant", referencedColumnName="id", onDelete="CASCADE")
     **/
    protected $encadrant;

    /**
     * @ORM\OneToOne(targetEntity="AheDemandeBudget", orphanRemoval=true)
     * @ORM\JoinColumn(name="demande_budget", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     **/
    protected $demandeBudget;

    /**
     * @ORM\OneToOne(targetEntity="AheDemandeEncadrement", orphanRemoval=true)
     * @ORM\JoinColumn(name="demande_encadrement", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     **/
    protected $demandeEncadrement;

    /**
     * @ORM\OneToOne(targetEntity="AheDemandeMateriel", orphanRemoval=true)
     * @ORM\JoinColumn(name="demande_materiel", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     **/
    protected $demandeMateriel;

    /**
     * @ORM\OneToOne(targetEntity="AheDemandeSalle", orphanRemoval=true)
     * @ORM\JoinColumn(name="demande_salle", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     **/
    protected $demandeSalle;

    /**
     * @ORM\OneToOne(targetEntity="AheValidation", orphanRemoval=true)
     * @ORM\JoinColumn(name="validation", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     **/
    protected $validation;


    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIntitule() {
        return $this->intitule;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule) {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCategorie() {
        return $this->categorie;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    /**
     * @return string
     */
    public function getTypeActivite() {
        return $this->typeActivite;
    }

    /**
     * @param string $typeActivite
     */
    public function setTypeActivite($typeActivite) {
        $this->typeActivite = $typeActivite;
    }

    /**
     * @return string
     */
    public function getVolumeHoraire() {
        return $this->volumeHoraire;
    }

    /**
     * @param string $volumeHoraire
     */
    public function setVolumeHoraire($volumeHoraire) {
        $this->volumeHoraire = $volumeHoraire;
    }

    /**
     * @return integer
     */
    public function getNombreMaxParticipants() {
        return $this->nombreMaxParticipants;
    }

    /**
     * @param integer $nombreMaxParticipants
     */
    public function setNombreMaxParticipants($nombreMaxParticipants) {
        $this->nombreMaxParticipants = $nombreMaxParticipants;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut() {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin() {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin($dateFin) {
        $this->dateFin = $dateFin;
    }

    /**
     * @return string
     */
    public function getInteretPedagogique() {
        return $this->interetPedagogique;
    }

    /**
     * @param string $interetPedagogique
     */
    public function setInteretPedagogique($interetPedagogique) {
        $this->interetPedagogique = $interetPedagogique;
    }

    /**
     * @return string
     */
    public function getResultatsAttendus() {
        return $this->resultatsAttendus;
    }

    /**
     * @param string $resultatsAttendus
     */
    public function setResultatsAttendus($resultatsAttendus) {
        $this->resultatsAttendus = $resultatsAttendus;
    }

    /**
     * @return mixed
     */
    public function getEncadrant() {
        return $this->encadrant;
    }

    /**
     * @param mixed $encadrant
     */
    public function setEncadrant($encadrant) {
        $this->encadrant = $encadrant;
    }

    /**
     * @return mixed
     */
    public function getDemandeBudget() {
        return $this->demandeBudget;
    }

    /**
     * @param mixed $demandeBudget
     */
    public function setDemandeBudget($demandeBudget) {
        $this->demandeBudget = $demandeBudget;
    }

    /**
     * @return mixed
     */
    public function getDemandeEncadrement() {
        return $this->demandeEncadrement;
    }

    /**
     * @param mixed $demandeEncadrement
     */
    public function setDemandeEncadrement($demandeEncadrement) {
        $this->demandeEncadrement = $demandeEncadrement;
    }

    /**
     * @return mixed
     */
    public function getDemandeMateriel() {
        return $this->demandeMateriel;
    }

    /**
     * @param mixed $demandeMateriel
     */
    public function setDemandeMateriel($demandeMateriel) {
        $this->demandeMateriel = $demandeMateriel;
    }

    /**
     * @return mixed
     */
    public function getDemandeSalle() {
        return $this->demandeSalle;
    }

    /**
     * @param mixed $demandeSalle
     */
    public function setDemandeSalle($demandeSalle) {
        $this->demandeSalle = $demandeSalle;
    }

    /**
     * @return mixed
     */
    public function getValidation() {
        return $this->validation;
    }

    /**
     * @param mixed $validation
     */
    public function setValidation($validation) {
        $this->validation = $validation;
    }
}
