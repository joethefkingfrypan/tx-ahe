<?php

namespace TxAhe\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ahe_salle")
 */
class AheDemandeSalle {

    function __construct($sallePrevisionnelle) {
        $this->sallePrevisionnelle = $sallePrevisionnelle;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $sallePrevisionnelle;

    /* +---------------------------------------------+ *
     * |               GETTERS/SETTERS               | *
     * +---------------------------------------------+ */

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSallePrevisionnelle() {
        return $this->sallePrevisionnelle;
    }

    /**
     * @param string $sallePrevisionnelle
     */
    public function setSallePrevisionnelle($sallePrevisionnelle) {
        $this->sallePrevisionnelle = $sallePrevisionnelle;
    }
}
