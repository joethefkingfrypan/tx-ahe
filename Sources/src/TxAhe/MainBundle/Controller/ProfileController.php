<?php

namespace TxAhe\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ProfileController extends DefaultController {

    public function displayDashboardAction() {
        return $this->render('TxAheMainBundle:Profile:dashboard.complete.html.twig');
    }
}