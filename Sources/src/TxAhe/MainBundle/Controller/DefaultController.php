<?php

namespace TxAhe\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \DateTime;

class DefaultController extends Controller {

    protected function hasAdminRights() {
        $currentUser = $this->get('security.context');
        return ($currentUser->isGranted('ROLE_ADMIN') || $currentUser->isGranted('ROLE_SUPER_ADMIN'));
    }

    protected function isStudent() {
        $currentUser = $this->get('security.context');
        return !($currentUser->isGranted('ROLE_PERSONNEL') || $currentUser->isGranted('ROLE_ADMIN') || $currentUser->isGranted('ROLE_SUPER_ADMIN'));
    }

    protected function getIdFromCurrentUser() {
        return $this->get('security.context')->getToken()->getUser()->getId();
    }

    protected function getStringFromCurrentUser() {
        $nom = $this->getUser()->getNom();
        $prenom = $this->getUser()->getPrenom();
        $username = $this->getUser()->getUsername();
        return "$prenom $nom ($username)";
    }

    protected function getDateFromString($date) {
        $find = array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'novembre', 'decembre');
        $replace = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $date = str_replace($find, $replace, strtolower($date));
        return new DateTime(date('Y/m/d', strtotime($date)));
    }

    protected function getTokenizedDateFrom($date) {
        setlocale(LC_ALL,'fr_FR.utf8','fra');
        return array (
            "mois" => gmstrftime("%#B",date_timestamp_get($date)),
            "jour" => gmstrftime("%d",date_timestamp_get($date))
        );
    }

    protected function getStringFromDate($date) {
        setlocale(LC_ALL,'fr_FR.utf8','fra');
        return gmstrftime("%d %#B %Y",date_timestamp_get($date));
    }

    protected function findEncadrantById($post) {
        $idEncadrant = $post->request->get('encadrant');
        $repository = $this->getDoctrine()->getRepository('TxAheMainBundle:User');
        return $repository->find($idEncadrant);
    }

    protected function sendNotificationByEmail($targetEmail, $subject, $view, $parameters) {

        //TEMP (for dev purposes)
        $targetEmail = "romgagnaire@gmail.com";

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('ahe-noreply@utt.fr')
            ->setTo($targetEmail)
            ->setBody(
                $this->renderView($view,$parameters),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    /**
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    protected function generateDatatable($view,$targetRoute,$validationProcess) {
        $controller_instance = $this;
        $datatable = $this->get('datatable');
        $datatable->setEntity("TxAheMainBundle:Ahe", "x")
            ->setFields(
                array(
                    "Intitule"          => "x.intitule",
                    "Type"              => "x.typeActivite",
                    "Categorie"         => "x.categorie",
                    "Volume Horaire"    => "x.volumeHoraire",
                    "Participants"      => "x.nombreMaxParticipants",
                    "Date de Début"     => "x.dateDebut",
                    "Date de Fin"       => "x.dateFin",
                    "_identifier_"      => "x.id" // you have to put the identifier field without label. Do not replace the "_identifier_"
                )
            )
            ->addJoin('x.validation', 'v', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
            ->setRenderer(
                function(&$data) use ($controller_instance) {
                    foreach ($data as $key => $value) {
                        if("object" == gettype($value)) {
                            setlocale(LC_TIME, "fr_FR");
                            $data[$key] = $this->getStringFromDate($value);
                        }
                    }
                }
            )
            ->setOrder("x.intitule", "asc")
            ->setRenderers(
                array(
                    7 => array(
                        'view' => $view,
                        'params' => array(
                            'details_route' => $targetRoute
                        ),
                    ),
                )
            )
            ->setHasAction(true);

        if($validationProcess) {
            $datatable->setWhere("x.validation IS NULL");
        } else {
            $datatable->setWhere("x.validation IS NOT NULL AND v.statut LIKE '%Validée%'");
        }

        return $datatable->execute();
    }
}