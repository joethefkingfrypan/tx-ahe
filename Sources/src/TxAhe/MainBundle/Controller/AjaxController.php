<?php

namespace TxAhe\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use TxAhe\MainBundle\Entity\Ahe;
use TxAhe\MainBundle\Entity\User;
use TxAhe\MainBundle\Entity\AheParticipation;
use TxAhe\MainBundle\Entity\AheValidation;

class AjaxController extends DefaultController {

    #+-----------------------------------------+
    #|       Application-related methods       |
    #+-----------------------------------------+

    public function applyForAheAction($id, Request $request) {
        if(!$request->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('tx_ahe_list_ahe'), 301);
        }
        try {

            // Retrieve all required data
            $em = $this->getDoctrine()->getManager();
            $post = Request::createFromGlobals();
            $commentaireParticipation = $post->request->get('commentaireParticipation');
            $ahe = $this->getDoctrine()->getRepository('TxAheMainBundle:Ahe')->find($id);
            $candidate = $this->get('security.context')->getToken()->getUser();

            // Create AheParticipation entity and persist it
            $participation = new AheParticipation($ahe,$candidate,$commentaireParticipation);
            $em->persist($participation);
            $em->flush();

            //Send the email
            $this->sendNewApplicationNotifications($ahe,$candidate,$commentaireParticipation);
            return new JsonResponse(array("code" => 100, "success" => true, "message" => array("Demande enregistrée")));
        } catch(\Exception $e) {
            $this->get('logger')->error("Une erreur est survenue lors de la création de demande de participation en BDD. Cause :" . $e->getMessage());
            return new JsonResponse(array("code" => 400, "success" => false, "message" => array("Une erreur est survenue lors de la postulation.","L'erreur est la suivante : " . $e->getMessage())));
        }
    }

    private function sendNewApplicationNotifications(Ahe $ahe, User $candidate,$commentaireParticipation) {

        // Retrieve all required data
        $id = $ahe->getId();
        $encadrant = $ahe->getEncadrant();
        $targetEmailEtudiant = $candidate->getEmail();
        $targetEmailEncadrant = $encadrant->getEmail();


        // Set view parameters
        $parameters = array(
            "ahe" => $ahe,
            "etudiant" => $candidate,
            "encadrant" => $encadrant,
            "dateDebut" => $this->getStringFromDate($ahe->getDateDebut()),
            "dateFin" => $this->getStringFromDate($ahe->getDateFin()),
            "url" => $this->generateUrl('tx_ahe_get_details_from_ahe', array('id' => $id)),
            "date" => $this->getTokenizedDateFrom(new \DateTime('now')),
            "commentaireParticipation" => $commentaireParticipation,
            "candidature" => $this->getDoctrine()->getRepository('TxAheMainBundle:AheParticipation')->getNumberOfParticipants($id)
        );

        // Send the email to student
        $this->sendNotificationByEmail(
            $targetEmailEtudiant,
            "Votre candidature a bien été envoyée",
            "TxAheMainBundle:Email:candidature.envoi.html.twig",
            $parameters
        );

        // Send the email to creator
        $this->sendNotificationByEmail(
            $targetEmailEncadrant,
            "Nouvelle candidature pour une de vos AHE",
            "TxAheMainBundle:Email:candidature.notification.html.twig",
            $parameters
        );
    }

    #+----------------------------------------+
    #|       Validation-related methods       |
    #+----------------------------------------+

    public function validateAheAction($id, Request $request) {
        if(!$request->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('tx_ahe_list_ahe'), 301);
        }
        try {

            // Retrieve all required data
            $em = $this->getDoctrine()->getManager();
            $post = Request::createFromGlobals();
            $currentTime = new \DateTime('now');
            $validated = $post->request->getBoolean('validated');

            // Create AheValidation entity with adequate data
            if($validated === true) {
                $commentaireValidation = $post->request->get('livrableSouhaite');
                $validation = new AheValidation("Validée",$commentaireValidation,$currentTime);
            } else {
                $commentaireValidation = $post->request->get('raisonsRejet');
                $validation = new AheValidation("Rejetée",$commentaireValidation,$currentTime);
            }

            // Persist entity and update related Ahe
            $em->persist($validation);
            $ahe = $this->getDoctrine()->getRepository('TxAheMainBundle:Ahe')->find($id);
            $ahe->setValidation($validation);
            $em->flush();

            // Send email notification to its creator
            $this->sendNewValidationNotification($validated,$ahe,$validation);
            return new JsonResponse(array("code" => 100, "success" => true, "message" => array("Demande enregistrée")));
        } catch(\Exception $e) {
            $this->get('logger')->error("Une erreur est survenue lors de la création de demande de participation en BDD. Cause :" . $e->getMessage());
            return new JsonResponse(array("code" => 400, "success" => false, "message" => array("Une erreur est survenue lors de la postulation.","L'erreur est la suivante : " . $e->getMessage())));
        }
    }

    private function sendNewValidationNotification($validated, Ahe $ahe, AheValidation $validation) {

        // Retrieve required data
        $id = $ahe->getId();
        $encadrant = $ahe->getEncadrant();
        $targetEmail = $encadrant->getEmail();

        // Define adequete email subject
        if($validated) {
            $subject = "Votre proposition d'AHE vient d'être validée";
        } else {
            $subject = "Votre proposition d'AHE a été refusée";
        }

        // Set parameters array
        $parameters = array(
            "ahe" => $ahe,
            "encadrant" => $encadrant,
            "validation" => $validation,
            "dateDebut" => $this->getStringFromDate($ahe->getDateDebut()),
            "dateFin" => $this->getStringFromDate($ahe->getDateFin()),
            "url" => $this->generateUrl('tx_ahe_get_details_from_ahe', array('id' => $id)),
            "date" => $this->getTokenizedDateFrom(new \DateTime('now'))
        );

        //Send the email
        $this->sendNotificationByEmail(
            $targetEmail,
            $subject,
            "TxAheMainBundle:Email:ahe.validation.html.twig",
            $parameters
        );
    }
}