<?php

namespace TxAhe\MainBundle\Controller;

class DetailsController extends DefaultController {

    public function getDetailsFromAheAction($id) {

        // Retrieve AHE entity from ID
        // If the ID is invalid, redirect user to the listing
        $ahe = $this->getDoctrine()->getRepository('TxAheMainBundle:Ahe')->find($id);
        if(!$ahe) {
            return $this->redirect($this->generateUrl('tx_ahe_list_ahe'), 301);
        }

        // Check if the user has access to this AHE
        $dateValidation = null;
        $encadrant = $ahe->getEncadrant();
        $validation = $ahe->getValidation();
        if(!$validation) {
            if(!$this->hasPermissionToSeeThisAhe($encadrant)) {
                return $this->redirect($this->generateUrl('tx_ahe_list_ahe'), 301);
            }
        } else {
            $dateValidation = $this->getStringFromDate($validation->getDateValidation());
        }

        // Retrieve required data
        $candidatures = null;
        $dateDebut = $this->getStringFromDate($ahe->getDateDebut());
        $dateFin = $this->getStringFromDate($ahe->getDateFin());
        $demande_budget = $ahe->getDemandeBudget();
        $demande_salle = $ahe->getDemandeSalle();
        $demande_materiel = $ahe->getDemandeMateriel();
        $demande_encadrement = $ahe->getDemandeEncadrement();

        // Adapt data to user rights/groups
        if($this->isStudent()) {
            $userId = $this->getIdFromCurrentUser();
            $candidatures = $this->getDoctrine()->getRepository('TxAheMainBundle:AheParticipation')->getApplicationStatus($userId,$id);
        } else {
            $candidatures = $this->getDoctrine()->getRepository('TxAheMainBundle:AheParticipation')->getNumberOfParticipants($id);
        }

        // Return view and all its parameters
        return $this->render('TxAheMainBundle:Ahe:ahe.details.html.twig',array(
            "id"             => $id,
            "ahe"            => $ahe,
            "dateDebut"      => $dateDebut,
            "dateFin"        => $dateFin,
            "encadrant"      => $encadrant,
            "budget"         => $demande_budget,
            "salle"          => $demande_salle,
            "materiel"       => $demande_materiel,
            "encadrement"    => $demande_encadrement,
            "candidature"    => $candidatures,
            "validation"     => $validation,
            "dateValidation" => $dateValidation
        ));
    }

    private function hasPermissionToSeeThisAhe($encadrant) {
        $encadrantID = $encadrant->getId();
        $userID = $this->getIdFromCurrentUser();
        return ($encadrantID == $userID) || $this->hasAdminRights();
    }
}