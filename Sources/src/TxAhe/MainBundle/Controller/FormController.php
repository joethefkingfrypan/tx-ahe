<?php

namespace TxAhe\MainBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use TxAhe\MainBundle\Entity\Ahe;
use TxAhe\MainBundle\Entity\AheDemandeBudget;
use TxAhe\MainBundle\Entity\AheDemandeEncadrement;
use TxAhe\MainBundle\Entity\AheDemandeMateriel;
use TxAhe\MainBundle\Entity\AheDemandeSalle;

class FormController extends DefaultController {

    public function createAheAction(Request $request) {
        if(!$request->isXmlHttpRequest()) {

            // Get all AHE categories from database
            $repository = $this->getDoctrine()->getRepository('TxAheMainBundle:AheCategorie');
            $categories = $repository->findAll();

            // Get all possible users from database
            $repository = $this->getDoctrine()->getRepository('TxAheMainBundle:User');
            $users = $repository->findAll();
            return $this->render('TxAheMainBundle:Ahe:ahe.create.html.twig', array('categories' => $categories, 'users' => $users));
        }

        // Validate form fields
        $post = Request::createFromGlobals();
        if (!$this->validateForm($post)) {
            return new JsonResponse(array("code" => 400, "success" => false, "message" => array("Le formulaire ne contient pas tous les éléments requis")));
        }

        try {
            $em = $this->getDoctrine()->getManager();

            // Create and persist all required entities
            $encadrant = $this->findEncadrantById($post);
            $demandeBudget =  $this->persistDemandeBudget($post,$em);
            $demandeSalle = $this->persistDemandeSalle($post,$em);
            $demandeMateriel = $this->persistDemandeMateriel($post,$em);
            $demandeEncadrement = $this->persistDemandeEncadrement($post,$em);
            $parameters = $this->persistAhe($post,$em,$encadrant,$demandeBudget,$demandeSalle,$demandeMateriel,$demandeEncadrement);
            $em->flush();

            // Log creation and send email notification
            $this->get('logger')->info("Nouvelle AHE créée par " . $this->getStringFromCurrentUser());
            $this->notify($parameters);

            return new JsonResponse(array("code" => 100, "success" => true, "message" => array("AHE sauvegardée")));
        } catch(\Exception $e) {
            $this->get('logger')->error("Une erreur est survenue lors de la sauvegarde d'une AHE en BDD. Cause :" . $e->getMessage());
            return new JsonResponse(array("code" => 400, "success" => false, "message" => array("Une erreur est survenue lors de la sauvegarde des données.","L'erreur est la suivante : " . $e->getMessage())));
        }
    }

    private function validateForm($post) {
        return
            $post->request->has('encadrant') &&
            $post->request->has('volumeHoraire') &&
            $post->request->has('nombreParticipants') &&
            $post->request->has('descriptif') &&
            $post->request->has('interetPedagogique') &&
            $post->request->has('resultatsAttendus') &&
            $post->request->has('intitule') &&
            $post->request->has('categorie') &&
            $post->request->has('type') &&
            $post->request->has('dateDebut') &&
            $post->request->has('dateFin') &&
            $post->request->has('besoinBudget') &&
            $post->request->has('besoinSalle') &&
            $post->request->has('besoinMateriel') &&
            $post->request->has('besoinEncadrement') &&
            $this->validateOptionalFormFields($post,'besoinBudget') &&
            $this->validateOptionalFormFields($post,'besoinSalle') &&
            $this->validateOptionalFormFields($post,'besoinMateriel') &&
            $this->validateOptionalFormFields($post,'besoinEncadrement');
    }

    private function validateOptionalFormFields($post, $switchButton) {
        $switchIsOff = ('off' == $post->request->get($switchButton));
        if($switchIsOff) {
            return true;
        }
        switch($switchButton) {
            case 'besoinBudget':
                return $post->request->has('budgetPrevisionnel') &&
                $post->request->has('detailsBudget');
            case 'besoinSalle':
                return $post->request->has('sallePrevisionnelle');
            case 'besoinMateriel':
                return $post->request->has('materielPrevisionnel');
            case 'besoinEncadrement':
                return $post->request->has('encadrementPrevisionnel') &&
                $post->request->has('budgetEncadrement') &&
                $post->request->has('volumeHoraireEncadrement');
            default:
                return false;
        }
    }

    private function persistDemandeBudget($post, ORM $em) {
        if('off' == $post->request->get('besoinBudget')){
            return null;
        }
        $budgetPrevisionnel = $post->request->get('budgetPrevisionnel');
        $detailsBudget = $post->request->get('detailsBudget');
        $demandeBudget = new AheDemandeBudget($budgetPrevisionnel,$detailsBudget);
        $em->persist($demandeBudget);
        return $demandeBudget;
    }

    private function persistDemandeSalle($post, ORM $em) {
        if('off' == $post->request->get('besoinSalle')){
            return null;
        }
        $sallePrevisionnelle = $post->request->get('sallePrevisionnelle');
        $demandeSalle = new AheDemandeSalle($sallePrevisionnelle);
        $em->persist($demandeSalle);
        return $demandeSalle;
    }

    private function persistDemandeMateriel($post, ORM $em) {
        if('off' == $post->request->get('besoinMateriel')){
            return null;
        }
        $materielPrevisionnel = $post->request->get('materielPrevisionnel');
        $demandeMateriel = new AheDemandeMateriel($materielPrevisionnel);
        $em->persist($demandeMateriel);
        return $demandeMateriel;
    }

    private function persistDemandeEncadrement($post, ORM $em) {
        if('off' == $post->request->get('besoinEncadrement')){
            return null;
        }
        $encadrementPrevisionnel = $post->request->get('encadrementPrevisionnel');
        $volumeHoraireEncadrement = $post->request->get('volumeHoraireEncadrement');
        $budgetEncadrement = $post->request->get('budgetEncadrement');
        $demandeEncadrement = new AheDemandeEncadrement($encadrementPrevisionnel,$volumeHoraireEncadrement,$budgetEncadrement);
        $em->persist($demandeEncadrement);
        return $demandeEncadrement;
    }

    private function persistAhe($post, ORM $em, $encadrant, $demandeBudget, $demandeSalle, $demandeMateriel, $demandeEncadrement) {

        // Set required data
        $intitule = $post->request->get('intitule');
        $categorie = $post->request->get('categorie');
        $type = $post->request->get('type');
        $volumeHoraire = $post->request->get('volumeHoraire');
        $nombreParticipantsMax = $post->request->get('nombreParticipants');
        $dateDebut = $this->getDateFromString($post->request->get('dateDebut'));
        $dateFin = $this->getDateFromString($post->request->get('dateFin'));

        // Create AHE entity and persist it
        $ahe = new Ahe();
        $ahe->setDescription($post->request->get('descriptif'));
        $ahe->setIntitule($intitule);
        $ahe->setCategorie($categorie);
        $ahe->setTypeActivite($type);
        $ahe->setVolumeHoraire($volumeHoraire);
        $ahe->setNombreMaxParticipants($nombreParticipantsMax);
        $ahe->setDateDebut($dateDebut);
        $ahe->setDateFin($dateFin);
        $ahe->setInteretPedagogique($post->request->get('interetPedagogique'));
        $ahe->setResultatsAttendus($post->request->get('resultatsAttendus'));
        $ahe->setEncadrant($encadrant);
        $ahe->setDemandeBudget($demandeBudget);
        $ahe->setDemandeSalle($demandeSalle);
        $ahe->setDemandeMateriel($demandeMateriel);
        $ahe->setDemandeEncadrement($demandeEncadrement);
        $em->persist($ahe);

        // Return array containing all view parameters
        // Note that you can't get AHE's ID until em->flush() has been called
        return array(
            "ahe" => $ahe,
            "dateDebut" => $this->getStringFromDate($dateDebut),
            "dateFin" => $this->getStringFromDate($dateFin),
            "encadrant" => $encadrant,
            "date" => $this->getTokenizedDateFrom(new \DateTime('now'))
        );
    }

    private function notify($partialParameters) {

        // Retrieve and set required data for view parameters
        $id = $partialParameters['ahe']->getId();
        $emailEncadrant = $partialParameters['encadrant']->getEmail();
        $parameters = array_merge_recursive($partialParameters,array(
            "url" => $this->generateUrl('tx_ahe_get_details_from_ahe', array('id' => $id))
        ));

        //Send the email
        $this->sendNotificationByEmail(
            $emailEncadrant,
            "Votre proposition d'AHE a bien été envoyée",
            "TxAheMainBundle:Email:ahe.envoi.html.twig",
            $parameters
        );
    }
}