<?php

namespace TxAhe\MainBundle\Controller;

use TxAhe\MainBundle\Entity\AheCategorie;
use TxAhe\MainBundle\Entity\User;

class DevController extends DefaultController {

    public function indexAction($name) {
        return $this->render('TxAheMainBundle:Ahe:index.html.twig', array('name' => $name));
    }

    public function createDbUsersAction() {
        $em = $this->getDoctrine()->getManager();
        $values = [
            new User("hilaring", "hilarion.nguimouth@utt.fr", "***", array('ROLE_USER'), "Etudiant", "Hilarion", "NGUIMOUTH", "ISI 3"),
            new User("gagnairr", "romain.gagnaire@utt.fr", "***", array('ROLE_USER'), "Etudiant", "Romain", "GAGNAIRE", "ISI 4"),
            new User("namyemil", "emilie.namy@utt.fr", "***", array('ROLE_PERSONNEL'), "Personnel UTT", "Emilie", "NAMY", null),
            new User("lenfantm", "muriel.lenfant@utt.fr", "***", array('ROLE_ADMIN'), "Enseignante", "Muriel", "LENFANT", null),
            new User("goudryst", "stephane.goudry@utt.fr", "***", array('ROLE_PERSONNEL'), "Personnel UTT", "Stéphane", "GOUDRY", null),
            new User("admin", "admin@utt.fr", "***", array('ROLE_SUPER_ADMIN'), "Administrateur Système", "Administrateur", "Système", null),
        ];
        try {
            foreach ($values as $currentUser) {
                $em->persist($currentUser);
                $em->flush();
            }
            $message = "<h3>" . count($values) . " enregistrements d'utilisateurs ont été crées avec succès.</h3>";
            return $this->render('TxAheMainBundle:Dev:dev.html.twig', array('title' => '[DEV] Utilisateurs', 'results' => $message));
        } catch(\Exception $e) {
            $message = "<h3>Les données demeurent inchangées.</h3><p>Les causes pouvant expliquer ceci sont les suivantes :</p><ul><li>Les données étaient déjà présentes en base</li><li>le service base de données n'est peut-être pas démarré.</li></ul>";
            return $this->render('TxAheMainBundle:Dev:dev.html.twig', array('title' => '[DEV] Catégories', 'results' => $message));
        }
    }

    public function createDbCategoriesAction() {
        $em = $this->getDoctrine()->getManager();
        $values = [
            new AheCategorie("Engagement pour l'UTT"),
            new AheCategorie("Engagement associatif"),
            new AheCategorie("Engagement personnel"),
            new AheCategorie("Activité scientifique ou pédagogique"),
            new AheCategorie("Professionalisation")
        ];
        try {
            foreach ($values as $currentCategorie) {
                $em->persist($currentCategorie);
                $em->flush();
            }
            $message = "<h3>" . count($values) . " enregistrements de catégories ont été crées avec succès.</h3>";
            return $this->render('TxAheMainBundle:Dev:dev.html.twig', array('title' => '[DEV] Catégories', 'results' => $message));
        } catch(\Exception $e) {
            $message = "<h3>Les données demeurent inchangées.</h3><p>Les causes pouvant expliquer ceci sont les suivantes :</p><ul><li>Les données étaient déjà présentes en base</li><li>le service base de données n'est peut-être pas démarré.</li></ul>";
            return $this->render('TxAheMainBundle:Dev:dev.html.twig', array('title' => '[DEV] Catégories', 'results' => $message));
        }
    }

    public function clearExistingAheAction() {
        $tables = [
            "AheDemandeBudget",
            "AheDemandeSalle",
            "AheDemandeMateriel",
            "AheDemandeEncadrement",
            "Ahe"
        ];
        $message = "<h3>Les tables suivantes ont bien été purgées</h3><ul>";
        $em = $this->getDoctrine()->getManager();
        foreach ($tables as $currentEntity) {
            $query = $em->createQuery("DELETE TxAheMainBundle:$currentEntity");
            $message .= "<li> " . $query->execute() . " enregistrements supprimé(s) dans $currentEntity</li>";
        }
        $message .= "</ul>";
        return $this->render('TxAheMainBundle:Dev:dev.html.twig', array('title' => 'Nettoyage BDD', 'results' => $message));
    }

    public function renderEmailAction() {
        $parameters = array(
            "ahe" => array(
                "id" => 1,
                "intitule" => "This is a test",
                "typeActivite" => "Collective",
                "categorie" => "Engagement pour l'UTT",
                "volumeHoraire" => "30h par semaine",
                "participantsMax" => 30,
                "commentaireValidation" => "Ceci est un exemple de commentaire de validation d\'AHE",
                "valide" => false
            ),
            "etudiant" => array(
                "nom" => "GAGNAIRE",
                "prenom" => "Romain",
                "email" => "gagnaire.romain@utt.fr",
                "cursus" => "ISI 4"
            ),
            "candidature" => array(
                "participants" => 4,
                "valide" => true,
                "commentaireValidation" => "Ceci est un exemple de commentaire de validation de candidature",
            ),
            "dateDebut" => "10 janvier 2015",
            "dateFin" => "14 janvier 2016",
            "url" => $this->generateUrl('tx_ahe_get_details_from_ahe', array('id' => 1)),
            "encadrant" => array(
                "nom" => "LENFANT",
                "prenom" => "Muriel",
                "email" => "muriel.lenfant@utt.fr"
            ),
            "date" => $this->getTokenizedDateFrom(new \DateTime('now'))
        );
        return $this->render('TxAheMainBundle:Email:candidature.validation.html.twig', $parameters);
    }
}