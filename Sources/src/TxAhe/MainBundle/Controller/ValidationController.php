<?php

namespace TxAhe\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ValidationController extends DefaultController {

    public function displayAheListAction() {
        try {
            $test = $this->generateDatatable('TxAheMainBundle:Datatable:standard.html.twig', 'tx_ahe_get_details_from_ahe', true);
            return $this->render('TxAheMainBundle:Ahe:ahe.validation.html.twig',array("test" => $test));
        } catch(\Exception $e) {
            $message = "<h3>La récupération de la liste des AHE en attente de validation a échoué.</h3><p>Les causes pouvant expliquer ceci sont les suivantes :</p><ul><li>le service de base de données n'est peut-être pas démarré.</li></ul>";
            return $this->render('TxAheMainBundle:Ahe:basic.html.twig', array('title' => 'Validation des AHE', 'results' => $message));
        }
    }

    public function retrieveDataAction(Request $request) {
        if(!$request->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('tx_ahe_list_validation'), 301);
        }
        return $this->generateDatatable('TxAheMainBundle:Datatable:administration.html.twig', 'tx_ahe_get_details_from_ahe', true);
    }
}