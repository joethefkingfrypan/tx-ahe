/*jslint browser: true*/
/*global $, jQuery, alert, Routing, console*/

$( document ).ready(function(){
    "use strict";

    function getValuesFromFormLivrable() {
        if(switchButtonIsOn("#besoinLivrable")) {
            return {
                "validated": true,
                "livrableSouhaite": $('#livrableSouhaite').val()
            }
        }
        return {
            "validated": true,
            "besoinLivrable": null
        }
    }

    function getValuesFromRejection() {
        return {
            "validated": false,
            "raisonsRejet": $('#raisonsRejet').val()
        }
    }

    function getValuesFromApplication() {
        return {
            "commentaireParticipation": $('#commentaireParticipation').val()
        }
    }

    function triggerAjaxRequest(buttons,loadingDiv,resultDiv,targetRoute,successMessage,errorMessage,data) {
        $.ajax({
            type: "POST",
            url: Routing.generate(targetRoute, { id: $('#target').val() }),
            data: data,
            dataType: "json",
            beforeSend: function() {
                loadingDiv.show();
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].hide();
                }
                $('#applyDiv').removeClass("standard-margin").addClass("reduced-margin");
            },
            success: function (data) {
                loadingDiv.hide();
                if (!data.success) {
                    resultDiv.html(generateAjaxSuccessAlert("warning",getErrorMessageFromResponse(data)));
                } else {
                    resultDiv.html(generateAjaxSuccessAlert("success",successMessage));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                loadingDiv.hide();
                resultDiv.html(generateAjaxSuccessAlert("error",errorMessage + " <p>[" + textStatus + "] " + errorThrown + "</p>"));
            }
        });
    }

    $('#mainBody').find(".textarea").each(function() {
        var $currentContent = $(this).html();
        $(this).html($currentContent.replace(/\r\n|\r|\n/g,"<br />"));
    });

    var loadingDiv, resultDiv, errorDiv, modalButton, form, validateButton, rejectionButton, buttons;
    loadingDiv = $("#loadingDiv");
    resultDiv = errorDiv = $('#resultDiv');
    validateButton = $('#validateButton');
    rejectionButton = $('#rejectionButton');
    buttons = [ validateButton, rejectionButton ];

    if (document.getElementById("validationModal")) {
        var besoinLivrable, optionalLivrable;
        besoinLivrable = $('#besoinLivrable');
        optionalLivrable = $('#optionalLivrable');
        modalButton = $('#modal-validate');
        errorDiv = $('#errorDiv');
        form = $('#msform');

        // Bootstrap Toggle
        initializeBootstrapToggle(besoinLivrable, "Oui", "Non");
        besoinLivrable.change(function() {
            optionalLivrable.fadeToggle();
            $('#livrableSouhaite').toggleClass("ignore");
        });

        // Form validation + AJAX request on submit
        modalButton.click(function(){
            resetErrorDiv(errorDiv);
            form.validate({
                validClass: 'success',
                ignore: '.ignore',
                rules: { livrableSouhaite: "required" },
                messages: { livrableSouhaite: { required: "Vous devez préciser quelle(s) salle(s) vous souhaiteriez." } },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    var errorId = "error" + elem.attr('id');
                    if(errorIdDoesNotExistsYet(errorId)) {
                        // Append error message only if it doesn't exist yet
                        errorDiv.append(generateValidationAlert(errorId,error));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.removeClass(errorClass).addClass(validClass);
                }
            });
            if (form.valid()) {
                $('#validationModal').modal('hide');
                triggerAjaxRequest(
                    buttons,
                    loadingDiv,
                    resultDiv,
                    "tx_ahe_validate",
                    "L'AHE a été validée avec succès.",
                    "Votre demande de validation n'a pas pu être enregistrée.",
                    getValuesFromFormLivrable()
                );
            }
        });
    }

    if (document.getElementById("rejectionModal")) {
        var raisonsRejet;
        raisonsRejet = $('#raisonsRejet');
        modalButton = $('#modal-invalidate');
        errorDiv = $('#errorDiv2');
        form = $('#msform2');

        modalButton.click(function(){
            form.validate({
                validClass: 'success',
                rules: { raisonsRejet: "required" },
                messages: { raisonsRejet: { required: "Vous devez préciser les raisons justifiant le rejet de cette proposition d'AHE." } },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    var errorId = "error" + elem.attr('id');
                    if(errorIdDoesNotExistsYet(errorId)) {
                        // Append error message only if it doesn't exist yet
                        errorDiv.append(generateValidationAlert(errorId,error));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.removeClass(errorClass).addClass(validClass);
                }
            });
            if (form.valid()) {
                $('#rejectionModal').modal('hide');
                triggerAjaxRequest(
                    buttons,
                    loadingDiv,
                    resultDiv,
                    "tx_ahe_validate",
                    "L'AHE a été invalidée avec succès.",
                    "Votre demande d'invalidation n'a pas pu être enregistrée.",
                    getValuesFromRejection()
                );
            }
        });
    }

    if (document.getElementById("applicationModal")) {
        var applyButton, commentaireParticipation;
        buttons = [ $('#applyButton') ];
        commentaireParticipation = $('#commentaireParticipation');
        modalButton = $('#modal-apply');
        errorDiv = $('#errorDiv');
        form = $('#msform');

        modalButton.click(function(){
            form.validate({
                validClass: 'success',
                rules: { commentaireParticipation: "required" },
                messages: { commentaireParticipation: { required: "Vous devez préciser vos motivations." } },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    var errorId = "error" + elem.attr('id');
                    if(errorIdDoesNotExistsYet(errorId)) {
                        // Append error message only if it doesn't exist yet
                        errorDiv.append(generateValidationAlert(errorId,error));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    elem.removeClass(errorClass).addClass(validClass);
                }
            });
            if (form.valid()) {
                $('#applicationModal').modal('hide');
                triggerAjaxRequest(
                    buttons,
                    loadingDiv,
                    resultDiv,
                    "tx_ahe_apply",
                    "Votre postulation pour cette AHE a été enregistrée avec succès.",
                    "Votre postulation n'a pas pu être enregistrée.",
                    getValuesFromApplication()
                );
            }
        });
    }
});