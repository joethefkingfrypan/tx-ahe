"use strict";

function generateValidationAlert(errorId,error) {
    return '<div id="' + errorId + '" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> ERREUR: ' + error.html() + '</div>';
}

function generateAjaxSuccessAlert(type,message) {
    switch(type) {
        case "success":
            return '<div class="alert alert-success" role="alert"><strong>' + message + '</strong></div>';
            break;
        case "warning":
            return '<div class="alert alert-warning" role="alert"><strong>' + message + '</strong></div>';
            break;
        case "danger":
            return '<div class="alert alert-danger" role="alert"><strong>' + message + '</strong></div>';
            break;
        case "error":
            return '<div class="alert alert-danger" role="alert"><strong>' + message + '</strong></div>';
            break;
        default:
            return '<div class="alert alert-info" role="alert"><strong>' + message + '</strong></div>';
            break;
    }
}

function initializeTouchspin(elem, initValue, minValue, maxValue, prefixValue) {
    if(typeof prefixValue === 'undefined') {
        elem.TouchSpin({
            verticalbuttons: true, verticalupclass: 'glyphicon glyphicon-plus', verticaldownclass: 'glyphicon glyphicon-minus', boostat: 5, maxboostedstep: 10,
            initval: initValue,
            min: minValue,
            max: maxValue
        });
        elem.parent().css('float','left');
    } else {
        elem.TouchSpin({
            verticalbuttons: true, verticalupclass: 'glyphicon glyphicon-plus', verticaldownclass: 'glyphicon glyphicon-minus', boostat: 5, maxboostedstep: 10,
            initval: initValue,
            min: minValue,
            max: maxValue,
            prefix: prefixValue
        });
    }
}

function initializeDatePicker(elem, minDateSetToNow) {
    if(minDateSetToNow) {
        elem.datetimepicker({
            locale: 'fr', showTodayButton: true, daysOfWeekDisabled: [0,6], format: 'DD MMMM YYYY',
            minDate: "now"
        });
    } else {
        elem.datetimepicker({
            locale: 'fr', showTodayButton: true, daysOfWeekDisabled: [0,6], format: 'DD MMMM YYYY'
        });
    }
}

function initializeSelectTwo(elem, placeholderValue) {
    elem.select2({
        placeholder: placeholderValue,
        allowClear: true
    });
}

function initializeBootstrapToggle(elem, valueOn, valueOff) {
    elem.bootstrapToggle({
        on: valueOn,
        off: valueOff,
        width: 200
    });
    elem.bootstrapToggle('off');
}

function resetErrorDiv(elem) {
    elem.html('');
}

function switchButtonIsOn(switchButton) {
    var parentWrapper = $(switchButton).parent();
    return !parentWrapper.hasClass('off');
}

function toggleIgnoreClassOnNextFormOptionalFields(switchButton, optionalDiv) {
    if(switchButtonIsOn(switchButton)) {
        $(optionalDiv).find(".toggleValidationOptional").toggleClass('ignore');
    }
}

function toggleIgnoreClassOnFieldsFromFieldset(elem) {
    elem.find(".toggleValidation").toggleClass('ignore');
    switch(elem.attr('id')) {
        case "formBudget":
            toggleIgnoreClassOnNextFormOptionalFields('#besoinBudget','#optionalBudget');
            break;
        case "formMateriel":
            toggleIgnoreClassOnNextFormOptionalFields('#besoinSalle','#optionalSalle');
            toggleIgnoreClassOnNextFormOptionalFields('#besoinMateriel','#optionalMateriel');
            break;
        case "formEncadrementSupplementaire":
            toggleIgnoreClassOnNextFormOptionalFields('#besoinEncadrement','#optionalEncadrement');
            break;
        default:
            break;
    }
}

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns results a new object based on obj1 and obj2
 */
function mergeJSON(obj1,obj2){
    var results = {};
    for (var attrname in obj1) { results[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { results[attrname] = obj2[attrname]; }
    return results;
}

function errorIdDoesNotExistsYet(errorId) {
    var elem = $('#' + errorId);
    return !elem.length;
}

function getErrorMessageFromResponse(data) {
    var errorMessage = "";
    for (var x = 0; x < data.message.length; x = x + 1) {
        errorMessage += "<p> - " + data.message[x] + "</p>";
    }
    return errorMessage;
}