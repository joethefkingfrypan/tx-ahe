/*jslint browser: true*/
/*global $, jQuery, alert, Routing, console*/

$(document).ready(function () {
    "use strict";

    function setClassesDependingOnDisabledStatus($elem) {
        if($elem.hasClass("ui-state-disabled")) {
            $elem.removeClass("ui-state-disabled").addClass("disabled");
        } else {
            $elem.addClass("hoverActivated");
        }
    }

    function setClassesDependingOnButtonPosition($elem) {
        if($elem.is(".first,.previous,.next,.last")) {
            $elem.addClass("paginate-blue");
        } else {
            $elem.addClass("paginate-lightblue");
        }
    }

    var first = true;
    var idDatatable = '#' + $('.table').attr('id');

    $(idDatatable).on( 'processing.dt', function () {
        if(first) {
            $('.dataTables_paginate').hide();
            first = false;
        }
    });

    $(idDatatable).on( 'init.dt', function () {
        $('.dataTables_paginate').show();
    });

    $(idDatatable).on( 'draw.dt', function () {
        $('.dataTables_paginate').find(".ui-button").each(function() {
            var $elem = $(this);
            $elem.addClass("paginate_button").addClass("custom-pagination");
            setClassesDependingOnDisabledStatus($elem);
            setClassesDependingOnButtonPosition($elem);
        });
    });
});