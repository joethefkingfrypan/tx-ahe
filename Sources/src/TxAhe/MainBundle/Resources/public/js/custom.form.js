/*jslint browser: true*/
/*global $, jQuery, alert, console*/

$(document).ready(function () {
    "use strict";

    function setupTouchspins() {
        var nbParticipants = $('#nombreParticipants');
        var budgetPrevisionnel = $('#budgetPrevisionnel');
        initializeTouchspin(nbParticipants, 1, 1, 100);
        initializeTouchspin(budgetPrevisionnel, 100, 1, 1000000, '€');
    }

    function setupDatePickers() {
        var dateDebut = $('#dateDebut');
        var dateFin = $('#dateFin');
        initializeDatePicker(dateDebut, true);
        initializeDatePicker(dateFin, false);
        dateDebut.on("dp.change", function (e) {
            dateFin.data("DateTimePicker").minDate(e.date);
        });
        dateFin.on("dp.change", function (e) {
            $('#dateDebut').data("DateTimePicker").maxDate(e.date);
        });
    }

    function setupSelects() {
        var select2Encadrant = $('#encadrant');
        var select2Categorie = $('#categorie');
        var select2Type = $('#type');
        initializeSelectTwo(select2Encadrant, "Encadrant");
        initializeSelectTwo(select2Categorie, "Catégorie");
        initializeSelectTwo(select2Type, "Type d'AHE");
        select2Encadrant.on('change', function() {
            $('#statut').html($('#encadrant').find(':selected').data('statut'));
        });
    }

    function setupBootstrapTogglesForBudget() {
        var besoinBudget = $('#besoinBudget');
        var optionalBudget = $('#optionalBudget');
        initializeBootstrapToggle(besoinBudget, "Oui", "Non");
        besoinBudget.change(function() {
            optionalBudget.fadeToggle();
            $('#budgetPrevisionnel').toggleClass("ignore");
            $('#detailsBudget').toggleClass("ignore");
        });
    }

    function setupBootstrapTogglesForMaterial() {
        var besoinSalle = $('#besoinSalle');
        var optionalSalle = $('#optionalSalle');
        var besoinMateriel = $('#besoinMateriel');
        var optionalMateriel=  $('#optionalMateriel');
        initializeBootstrapToggle(besoinSalle, "Oui", "Non");
        initializeBootstrapToggle(besoinMateriel, "Oui", "Non");
        besoinSalle.change(function() {
            optionalSalle.fadeToggle();
            $('#sallePrevisionnelle').toggleClass("ignore");
        });
        besoinMateriel.change(function() {
            optionalMateriel.fadeToggle();
            $('#materielPrevisionnel').toggleClass("ignore");
        });
    }

    function setupBootstrapTogglesForExtraStaff() {
        var besoinEncadrement = $('#besoinEncadrement');
        var optionalEncadrement = $('#optionalEncadrement');
        initializeBootstrapToggle(besoinEncadrement, "Oui", "Non");
        besoinEncadrement.change(function() {
            optionalEncadrement.fadeToggle();
            $('#encadrementPrevisionnel').toggleClass("ignore");
            $('#volumeHoraireEncadrement').toggleClass("ignore");
            $('#budgetEncadrement').toggleClass("ignore");
        });
    }

    function setupComponents() {
        setupTouchspins();
        setupDatePickers();
        setupSelects();
        setupBootstrapTogglesForBudget();
        setupBootstrapTogglesForMaterial();
        setupBootstrapTogglesForExtraStaff();
    }

    function addErrorClassToUnvalidElements(elem,validClass,errorClass,specialErrorClass) {
        if (elem.hasClass("select2-offscreen")) {
            var select2Offscreen = $("#s2id_" + elem.attr("id") + " ul");
            select2Offscreen.addClass(errorClass).removeClass(validClass);
        } else {
            elem.addClass(errorClass).removeClass(validClass);
            if(elem.hasClass("prefixedTouchspin")) {
                var prefix = elem.prev();
                var buttonPlus = elem.parent().find(".bootstrap-touchspin-up");
                var buttonMinus = elem.parent().find(".bootstrap-touchspin-down");
                prefix.addClass(errorClass).removeClass(validClass);
                buttonPlus.addClass(errorClass).removeClass(specialErrorClass);
                buttonMinus.addClass(errorClass).removeClass(specialErrorClass);
            }
        }
    }

    function removeErrorClassFromUnvalidElements(elem,validClass,errorClass,specialErrorClass) {
        if (elem.hasClass("select2-offscreen")) {
            var select2Offscreen = $("#s2id_" + elem.attr("id") + " ul");
            select2Offscreen.removeClass(errorClass).addClass(validClass);
        } else {
            elem.removeClass(errorClass).addClass(validClass);
            // Remove classes from surrounding prefix/postfix elems if current element is a touchspin
            if(elem.hasClass("prefixedTouchspin")) {
                var prefix = elem.prev();
                var buttonPlus = elem.parent().find(".bootstrap-touchspin-up");
                var buttonMinus = elem.parent().find(".bootstrap-touchspin-down");
                prefix.removeClass(errorClass).addClass(validClass);
                buttonPlus.removeClass(errorClass).addClass(specialErrorClass);
                buttonMinus.removeClass(errorClass).addClass(specialErrorClass);
            }
        }
    }

    function getValuesFromFormEncadrant() {
        return {
            "encadrant" : $("#encadrant").val()
        }
    }

    function getValuesFromFormDescriptif() {
        return {
            "volumeHoraire" : $("#volumeHoraire").val(),
            "nombreParticipants" : $('#nombreParticipants').val(),
            "descriptif" : $('#descriptif').val(),
            "interetPedagogique" : $('#interetPedagogique').val(),
            "resultatsAttendus" : $('#resultatsAttendus').val()
        }
    }

    function getValuesFromFormAhe() {
        return {
            "intitule": $('#intitule').val(),
            "categorie" : $('#categorie').val(),
            "type" : $('#type').val(),
            "dateDebut" : $('#dateDebut').val(),
            "dateFin" : $('#dateFin').val()
        }
    }

    function getValuesFromFormBudget() {
        if(switchButtonIsOn("#besoinBudget")) {
            return {
                "besoinBudget": $('#besoinBudget').val(),
                "budgetPrevisionnel" : $('#budgetPrevisionnel').val(),
                "detailsBudget" : $('#detailsBudget').val()
            }
        }
        return {
            "besoinBudget": "off"
        }
    }

    function getValuesFromFormSalle() {
        if(switchButtonIsOn("#besoinSalle")) {
            return {
                "besoinSalle": $('#besoinSalle').val(),
                "sallePrevisionnelle" : $('#sallePrevisionnelle').val()
            }
        }
        return {
            "besoinSalle": "off"
        }
    }

    function getValuesFromFormMateriel() {
        if(switchButtonIsOn("#besoinMateriel")) {
            return {
                "besoinMateriel": $('#besoinMateriel').val(),
                "materielPrevisionnel" : $('#materielPrevisionnel').val()
            }
        }
        return {
            "besoinMateriel": "off"
        }
    }

    function getValuesFromFormEncadrementSupplementaire() {
        if(switchButtonIsOn("#besoinEncadrement")) {
            return {
                "besoinEncadrement": $('#besoinEncadrement').val(),
                "encadrementPrevisionnel" : $('#encadrementPrevisionnel').val(),
                "budgetEncadrement": $('#budgetEncadrement').val(),
                "volumeHoraireEncadrement" : $('#volumeHoraireEncadrement').val()
            }
        }
        return {
            "besoinEncadrement": "off"
        }
    }

    function getValuesFromEntireForm() {
        var results = {};
        var arrayFromValues = [
            getValuesFromFormEncadrant(),
            getValuesFromFormDescriptif(),
            getValuesFromFormAhe(),
            getValuesFromFormBudget(),
            getValuesFromFormSalle(),
            getValuesFromFormMateriel(),
            getValuesFromFormEncadrementSupplementaire()
        ];
        for(var currentJsonObject in arrayFromValues) {
            results = mergeJSON(results,arrayFromValues[currentJsonObject]);
        }
        return results;
    }

    function triggerAjaxRequestOnSubmit() {
        var loadingDiv = $("#simpleLoading");
        var subtitle = $('#confirmationSubtitle');
        var optionnalButton = $('#optionalButtons');
        var resultDiv = $('#resultDiv');

        $.ajax({
            type: "POST",
            url: "/ahe/creer",
            dataType: "json",
            data: getValuesFromEntireForm(),
            beforeSend: function() {
                loadingDiv.show();
                optionnalButton.hide();
                subtitle.text("Vos données sont en cours de traitement");
                resultDiv.html("");
            },
            success: function (data) {
                loadingDiv.hide();
                subtitle.text("Traitement terminé");
                if (!data.success) {
                    optionnalButton.show();
                    resultDiv.html(generateAjaxSuccessAlert("warning",getErrorMessageFromResponse(data)));
                } else {
                    resultDiv.html(generateAjaxSuccessAlert("success","Votre AHE a été créée avec succès !"));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                loadingDiv.hide();
                optionnalButton.show();
                subtitle.text("Une erreur est survenue");
                resultDiv.html(generateAjaxSuccessAlert("error","Votre AHE n'a pas pu être créée : " + textStatus + ". " + errorThrown));
            }
        });
    }

    function triggerAnimationNext(current_fs,next_fs) {
        toggleIgnoreClassOnFieldsFromFieldset(next_fs);
        current_fs.hide();
        next_fs.show();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset and hide the current fieldset with style
        next_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                var scale = 1 - (1 - now) * 0.2;    //1. scale current_fs down to 80%
                var left = (now * 50)+"%";          //2. bring next_fs from the right(50%)
                var opacity = 1 - now;              //3. increase opacity of next_fs to 1 as it moves in
                current_fs.css({'transform': 'scale('+scale+')'});
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
            },
            easing: 'easeInOutBack'
        });
        return false;
    }

    function triggerAnimationPrevious(errorDiv,current_fs,previous_fs) {
        resetErrorDiv(errorDiv);
        toggleIgnoreClassOnFieldsFromFieldset(current_fs);
        current_fs.hide();
        previous_fs.show();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset and hide the current fieldset with style
        previous_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                var scale = 0.8 + (1 - now) * 0.2;  //1. scale previous_fs from 80% to 100%
                var left = ((1-now) * 50)+"%";      //2. take current_fs to the right(50%) - from 0%
                var opacity = 1 - now;              //3. increase opacity of previous_fs to 1 as it moves in
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
            },
            easing: 'easeInOutBack'
        });
        return false;
    }

    /* +----------------------------------------+ *
     * |                JS LOGIC                | *
     * +----------------------------------------+ */

    setupComponents();
    var form = $("#msform");
    var errorDiv = $('#errorDiv');
    var current_fs, next_fs, previous_fs;   //fieldsets
    var animating;                          //flag to prevent quick multi-click glitches
    var specialErrorClass = "successLeftBorder";

    $(".next").click(function(){
        resetErrorDiv(errorDiv);
        form.validate({
            validClass: 'success',
            ignore: '.ignore',
            rules: {
                encadrant: "required",
                type: "required",
                categorie: "required",
                intitule: "required",
                dateDebut: "required",
                dateFin: "required",
                volumeHoraire: "required",
                nombreParticipants: "required",
                descriptif: "required",
                interetPedagogique: "required",
                resultatsAttendus: "required",
                budgetPrevisionnel: "required",
                detailsBudget: "required",
                sallePrevisionnelle: "required",
                materielPrevisionnel: "required",
                encadrementPrevisionnel: "required",
                volumeHoraireEncadrement: "required",
                budgetEncadrement: "required"
            },
            messages: {
                encadrant: { required: "Vous devez choisir un encadrant." },
                type: { required: "Vous devez choisir un type d'AHE." },
                categorie: { required: "Vous devez choisir une catégorie." },
                intitule: { required: "Vous devez renseigner l'intitulé." },
                dateDebut: { required: "Vous devez choisir une date de début." },
                dateFin: { required: "Vous devez choisir une date de fin." },
                volumeHoraire: { required: "Vous devez renseigner le volume horaire." },
                descriptif: { required: "Vous devez remplir le descriptif." },
                interetPedagogique: { required: "Vous devez décrire quel est l'intérêt pédagogique de l'AHE." },
                resultatsAttendus: { required: "Vous devez décrire les résultats attendus." },
                detailsBudget: { required: "Vous devez décrire la répartition du budget." },
                sallePrevisionnelle: { required: "Vous devez précisez quelle(s) salle(s) vous souhaiteriez." },
                materielPrevisionnel: { required: "Vous devez précisez quel matériel vous souhaiteriez." },
                encadrementPrevisionnel: { required: "Vous devez précisez quel est l'encadrement prévisionnel." },
                volumeHoraireEncadrement: { required: "Vous devez renseigner le volume horaire lié à l'encadrement." },
                budgetEncadrement: { required: "Vous devez renseigner le budget lié à l'encadrement." }
            },
            errorPlacement: function (error, element) {
                var elem = $(element);
                var errorId = "error" + elem.attr('id');
                if(errorIdDoesNotExistsYet(errorId)) {
                    // Append error message only if it doesn't exist yet
                    errorDiv.append(generateValidationAlert(errorId,error));
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                addErrorClassToUnvalidElements(elem, validClass, errorClass, specialErrorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                removeErrorClassFromUnvalidElements(elem, validClass, errorClass, specialErrorClass);
            }
        });
        if (form.valid()){
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            animating = triggerAnimationNext(current_fs,next_fs);
        }
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        animating = triggerAnimationPrevious(errorDiv,current_fs,previous_fs);
    });

    $("#submitForm").click(function(){
        // Submit button acts like any other "next" button
        // But also triggers an Ajax Request
        if (form.valid()) {
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            animating = triggerAnimationNext(current_fs, next_fs);
            triggerAjaxRequestOnSubmit();
        }
    });
});