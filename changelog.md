#### 13/03/2015
    
- Repo initialization (base files and folders)
- Introduced README to keep track of what's happening
- Introduced text file containing summary/ideas/stuff

#### 14/03/2015

- Created Symfony 2 project
- Added step-by-step guide to configure everything

#### 15/03/2015

- Added MopaBootstrapBundle
- Added FOSUserBundle
- Added documentation about Symfony bundles and step-by-step guide on how to install them