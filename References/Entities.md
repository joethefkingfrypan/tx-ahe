### Introduction

##### Scénario

- Création d'une `proposition d'AHE` par un étudiant ou un personnel de l'UTT.
- Après validation par comité, cette proposition devient un `sujet effectif`.
- Les étudiants peuvent alors postuler sur ce sujet afin de s'y `inscrire`.
- A la fin du semestre, évaluation des étudiants inscrits avec attribution de `points AHE`.

##### NOTES

- Le terme `AHE` est préférable à `proposition d'AHE` dans le sens où l'unique différence entre les deux réside dans son état *(validée ou non)*.

### Entité : `Utilisateur`

| Attributs             | Type de donnée                    | Exemple   |
|:---------------------:|:---------------------------------:|:---------:|
| ID                    | Nombre                            | 85541     |
| Nom                   | Texte                             | GAGNAIRE  |
| Prénom                | Texte                             | Romain    |
| Nom d'utlisateur      | Texte                             | gagnairr  |
| Statut                | `enseignant` ou `étudiant`        | Etudiant  |

- Tous les attributs ne sont pas indiqués ici. D'autres sont intégrés au système de gestion d'utilisateurs de symfony *(mot de passe, salt, last_visited, ...)*

### Entité : `Participation`

| Attributs     | Type de donnée    | Exemple   |
|:-------------:|:-----------------:|:---------:|
| ID Etudiant   | Nombre            | 85541     |
| ID AHE        | Nombre            | 538       |
| Résultat      | Nombre            | 85        |

- L'identifiant de cette relation est porté par le couple (`ID Etudiant`, `ID AHE`), il ne peut donc pas avoir deux tuples identiques.
- Il est également simple d'extraire les catégories d'AHE déjà effectuées par un étudiant (parcours de cette table en cherchant un ID étudiant spécifique, et en analysant tous les ID AHE associés).
- Le nombre de points est `NULL` jusqu'à évaluation de l'AHE

### Entité : `AHE`

| Attributs             | Type de donnée                            | Exemple                                       |
|:---------------------:|:-----------------------------------------:|:---------------------------------------------:|
| ID                    | Nombre                                    | 1649                                          |
| Encadrant             | Relation (`ID` de l'encadrant)            | 224                                           |
| Intitulé              | Texte                                     | Aide service cultures et initiatives          |
| Description           | Texte                                     | Aide à la préparation des événementiels, de l’accueil aux vernissages et installations des expositions, etc. |
| Catégorie             | Texte                                     | Engagement pour l'UTT                         |
| Type d'activité       | `individuelle` ou `collective`            | Collective                                    |
| Volume horaire        | Texte `?`                                 | Selon aide                                    |
| Nombre max participants | Nombre                                  | 4                                             |
| Date Début            | Date                                      | 1 septembre 2014                              |
| Date Fin              | Date                                      | 30 septembre 2020                             |
| Intérêt pédagogique   | Texte                                     | Gestion d’un budget conséquent (130 000€), Gestion de la sécurité, Maitrise des risques, ... |
| Résultats attendus    | Texte                                     | Diplôme officiel des Premiers Secours en Equipe niveau 1 et/ou 2 pour chaque participant.    |
| Validation            | Relation (`ID` de la validation)          | 25                                            |
| Demande budget        | Relation (`ID` de la demande budget)      | 1                                             |
| Demande encadrement   | Relation (`ID` de la demande encadrement) | 4                                             |
| Demande matériel      | Relation (`ID` de la demande matériel)    | 15                                            |

- A partir de l'`ID` de l'encadrant se déduit son nom, son prénom ainsi que son statut (étudiant, enseignant).
- Exemple : ID=224, Muriel LENFANT, Enseignante.

### Entité : `AHE_demande_budget`

| Attributs           | Type de donnée       | Exemple                                   |
|:-------------------:|:--------------------:|:-----------------------------------------:|
| ID                  | Nombre               | 1                                         |
| Budget Prévisionnel | Nombre (Unité : `€`) | 220€                                      |
| Détails du Budget   | Texte                | Guide Arduino : 20€. Carte Arduino : 200€ |

### Entité : `AHE_demande_encadrement`

| Attributs                  | Type de donnée | Exemple                                |
|:--------------------------:|:--------------:|:--------------------------------------:|
| ID                         | Nombre         | 4                                      |
| Encadrants Prévus          | Texte          | Formateur FFSS                         |
| Volume Horaire encadrement | Texte          | Au moins 7h par semaine                |
| Budget encadrement         | Texte          | 173 € (PSE1) , 197 € (PSE2) / personne |

### Entité : `AHE_demande_matériel`

| Attributs         | Type de donnée | Exemple             |
|:-----------------:|:--------------:|:-------------------:|
| ID                | Nombre         | 15                  |
| Matériel souhaité | Texte          | Rétro-projecteur    |
| Salle souhaitée   | Texte          | M104 / A déterminer |

### Entité : `AHE_validation`

| Attributs             | Type de donnée                    | Exemple                                       |
|:---------------------:|:---------------------------------:|:---------------------------------------------:|
| ID_AHE                | Nombre                            | 1855                                          |
| Date Validation       | Date                              | NULL                                          |
| Demande de Livrable   | Texte                             | NULL                                          |


