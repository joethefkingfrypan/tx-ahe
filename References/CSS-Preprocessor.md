## Configuration de l'environnement Less pour Symfony

#### Etape 1 - Installer un interpréteur Node.JS

1. Télécharger NodeJS depuis le [site officiel](https://nodejs.org/)
2. Ajouter le chemin d'installation de NodeJS à votre variable d'environnement PATH (par défaut : `C:\Program Files\nodejs`).

#### Etape 2 - Installer Less

1. Ouvrir un terminal
2. Se rendre dans le dossier Sources du projet
2. Exécuter `npm install less --prefix app/Resources`

##### Etape 3 - Installer UglifyJS

1. Ouvrir un terminal
2. Se rendre dans le dossier Sources du projet
2. Exécuter `npm install uglify-js --prefix app/Resources`

##### Etape 4 - Configurer Symfony avec les chemins appropriés

config.yml
```
    assetic:
        filters:
            cssrewrite:     ~
            uglifyjs2:
                bin:        "%node.module.path%"
                apply_to:   "\.js$"
            less:
                node:       "%node.path%"
                node_paths: "%node.module.path%"
                apply_to:   "\.less$"
```

parameters.yml
```
    node.path: 'C:\Program Files\nodejs\node.exe'
    node.module.path:
        - '%kernel.root_dir%/Resources/node_modules'
```