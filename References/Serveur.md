## Lancer le serveur Symfony en mode DEV

1. Ouvrir une console
2. Se rendre dans le dossier Sources du projet
3. Exécuter `php app/console cache:clear`
4. Exécuter `php app/console assets:install`
5. Exécuter `php app/console assetic:dump`
6. Exécuter `php app/console server:run`

## Lancer le serveur Symfony en mode PROD

1. Ouvrir une console
2. Se rendre dans le dossier Sources du projet
3. Exécuter `php app/console cache:clear --no-debug -e prod`
4. Exécuter `php app/console assets:install --no-debug -e prod`
5. Exécuter `php app/console assetic:dump --no-debug -e prod`
6. Exécuter `php app/console server:run -e prod`