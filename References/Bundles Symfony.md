## Symfony Bundles

Symfony présente l'avantage d'avoir un catalogue d'outils créés par la communauté qui évite d'avoir à développer des fonctionnalités qui sont récurrentes _(comme par exemple un système d'authentification)_. 

Ce catalogue est consultable à cette adresse : http://knpbundles.com/

Les bundles choisis sont les suivants :

- [Mopa Bootstrap Bundle](http://knpbundles.com/phiamo/MopaBootstrapBundle) : Intégration de Bootstrap dans Symfony 2
- [FOS User Bundle](http://knpbundles.com/FriendsOfSymfony/FOSUserBundle) : Gestion des utilisateurs avec parefeu

## Méthodes d'installation

##### Mopa Bootstrap Bundle

1. Se rendre dans le dossier contenant le fichier `composer.phar`
2. Exécuter `php composer.phar require mopa/bootstrap-bundle twbs/bootstrap`

##### FOS User Bundle

1. Se rendre dans le dossier contenant le fichier `composer.phar`
2. Exécuter `php composer.phar require friendsofsymfony/user-bundle "~2.0@dev"`
3. Activer le nouveau paquet dans le kernel en ajoutant les lignes suivantes dans le fichier `app/AppKernel.php` :

```
    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new FOS\UserBundle\FOSUserBundle(),
        );
    }
```
4. Editer le fichier `app/config/security.yml`

```
    # app/config/security.yml
    security:
        encoders:
            FOS\UserBundle\Model\UserInterface: sha512

        role_hierarchy:
            ROLE_ADMIN:       ROLE_USER
            ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

        providers:
            fos_userbundle:
                id: fos_user.user_provider.username

        firewalls:
            main:
                pattern: ^/
                form_login:
                    provider: fos_userbundle
                    csrf_provider: form.csrf_provider
                logout:       true
                anonymous:    true

        access_control:
            - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/, role: ROLE_ADMIN }
```
5. Rajouter à la fin du fichier `app/config/config.yml` les lignes suivantes :

```
    fos_user:
        db_driver: orm
        firewall_name: main
        user_class: Acme\DemoBundle\Entity\User
```
6. Rajouter les lignes suivantes à la suite du fichier `app/config/routing.xml` :

```
    fos_user:
        resource: "@FOSUserBundle/Resources/config/routing/all.xml"
```
7. Créer une entité `User` dans le bundle souhaité (pour l'exemple dans `DemoBundle`) :

```
    <?php   //src/Acme/DemoBundle/Entity/User.php

    namespace Acme\DemoBundle\Entity;

    use FOS\UserBundle\Model\User as BaseUser;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="fos_user")
     */
    class User extends BaseUser
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        public function __construct()
        {
            parent::__construct();
            // your own logic
        }
    }
```
8. Créer la base de données en exécutant la commande `php app/console doctrine:database:create`
10. Mettre à jour le schéma de la base en éxécutant la commande `php app/console doctrine:schema:update --force`
