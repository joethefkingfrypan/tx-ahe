## Références

- [Choose the right PHP framework](http://www.creativebloq.com/design/choose-right-php-framework-12122774)
- [Quel framework PHP pour 2014 ?](http://blog.nicolashachet.com/technologies/php/quel-framework-php-pour-2014/)
- [Comparatif des frameworks PHP](http://socialcompare.com/fr/comparison/php-frameworks-comparison)
- [Best PHP Frameworks for 2014](http://www.sitepoint.com/best-php-frameworks-2014/)
- [Ask Christoph: Symfony vs. Laravel?](https://www.christophh.net/2014/08/20/ask-christoph-symfony-over-laravel/)
- [http://www.reddit.com/r/PHP/comments/1bdln1/symfony2_vs_laravel](http://www.reddit.com/r/PHP/comments/1bdln1/symfony2_vs_laravel)

## Choix technologique

Le rapport de `Thomas Tabary` présentait trois technologies principales :

- PHP.
- Python.
- Ruby.

On pourrait également mentionner que la création de services Web est également réalisable dans les langages suivants :

- Javascript _(avec Node.JS)_.
- Java.

Cette phase de développement se veut être la création d'un `prototype fonctionnel`. Dans l'optique d'une éventuelle reprise par un autre étudiant dans les semestres à venir, il semble de judicieux de se limiter aux langages qui sont non seulement les plus courants mais également enseignés à l'UTT, sous peine de sévèrement restreindre le nombre de prétendants potentiels.

En effet, pour Java et PHP, seul `l'apprentissage du cadriciel` utilisé est requis. Alors que pour les langages que je qualifierais de "moins conventionnels", cela impose non seulement de se familiariser avec les `bases du langage` avec de pouvoir prendre pleine mesure de tel ou tel cadriciel.

## Comparaisons des framework PHP

#### 1. Cake PHP

Pros :

- Active community
- Cake Bakery (bakery. cakephp.org)

Cons : 

- Cake has very specific ways of doing things

This framework takes inspiration from Ruby on Rails and very much belongs to the KISS (keep it simple stupid) camp. Its framework structure is easy to understand. There are folders for controllers, models and views. Most web apps will have most of its code contained within these three folder types.

The highlight of CakePHP is the Cake Bakery where third-party developers can add their own code and share it with the community. CakePHP is a good middle ground, not as simple as CodeIgniter, but not quite as complex as Zend or Symfony.

CakePHP doesn’t have add-ons such as ORM or a template engine. Out of the four frameworks, this is the only one that doesn’t have corporate backing – but that could be seen as a plus point.

#### 2. Code Igniter

Pros :

- Speed
- Very lightweight
- Large active community
- Good documentation
- (+) great docs, lots of users, lots of info, has all I need;

Cons : 

- Can allow too much freedom in coding
- (-) lots of what I don’t need; old PHP4 support means more and uglier code (?), people say it’s dead

Created by EllisLab in Oregon and released soon after CakePHP, in 2006, this is great in projects where a best of breed approach is favoured. You can add other third-party software without feeling weighted down, and like CakePHP and Zend, it has a clear MVC folder structure which you can follow. CodeIgniter also gives developers more headroom to solve problems according to their thinking, unlike Zend or Symfony, in which you need to think in terms of the individual methodology.

#### 3. Symfony

Pros :

- ORM
- Has its own template engine

Cons : 

- Steep learning curve

Symfony, created by the French agency SensioLabs, definitely feels different. It promotes using ORM, a plus for heavy database work, and uses its own Twig template engine, also created by SensioLabs.

To get a feel, head to symfony.com, the version 2 site. Don’t get confused with the version 1 site, symfony-project.org; version 2 is the one to learn!

There isn’t an obvious MVC terminology within Symfony, though it does much the same thing as the other frameworks. It has a unique concept called Bundles, collections of related code or files that help separate web app features. Like Zend, code generation plays an important part in Symfony.

This is the most difficult framework to grasp, but with good community support, help is never far away. The model structure is different to many other frameworks: it promotes Doctrine as the entry point for the business logic model code.

#### 4. Zend

Pros :

- Good corporate software with longevity

Cons : 

- Design pattern concepts take time to grasp

Created by Zend, the firm behind the PHP engine, it was obvious this framework would be taken seriously. Zend has the features of a robust, corporate style software as well as related commercial products such as a commercial web server, support and its own IDE. In combination these make it a powerful way to develop web apps.

Zend is similar to CodeIgniter and CakePHP, but heavy documentation based on design pattern concepts can make it hard to get started with.

Zend is probably the most sought after framework when looking at PHP-related job descriptions, and has quite a few good features within it. The Lucene search feature brings commercial grade web search to an app; other highlights include form creation, data filtering and Internationalisation. But Zend is much more than a PHP framework; you benefit from the support, training, certification and related products.

## Décision finale

Après examen des différences en Laravel et Symfony, mon choix s'est porté sur ce dernier, pour les raisons suivantes :

- Sa connaissance en fait un atout professionnel de poids puisqu'il fait partie des plus usités.
- Il dispose d'un ORM _(système de gestion de bases de données simplifié)_, d'un moteur de template _(création d'interface graphique simplifié)_, tout en étant très facilement modulable grâce à son catalogue d'extensions.
- J'ai déjà pu en faire l'expérience dans l'UV LO07 _(contrairement à Laravel)_.