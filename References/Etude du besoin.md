## I - Explications

### Processus de création d'une nouvelle AHE

La proposition d'une nouvelle AHE peut se faire :
- par un `étudiant`.
- par une `association d'étudiants`.
- par un `personnel de l'UTT` _(qu'il soit enseignant ou administratif)_ .

La proposition se fait par remplissage d'un __formulaire sur Moodle__ qui aboutit à la création d'une __fiche de proposition__. Les propositions seront examinées par le `comité AHE`, qui se réunit plusieurs fois par semestre.

Si une proposition est validée, la fiche sera alors rendue __visible__ sur Moodle et les étudiants pourront ensuite y postuler.

__[RÉSUMÉ]__ Les étapes sont donc les suivantes :

1. Proposition d'une AHE par un membre de l'UTT _(création d'une fiche de proposition)_.
2. Proposition examinée par le comité AHE.
3. En cas de validation, le sujet d'AHE est rendue disponible aux étudiants _(la fiche devient visible)_.
4. Les étudiants peuvent alors candidater.

#### Processus de réalisation d'une AHE

La première étape réside dans la postulation d'un `candidat` pour une AHE par prise de contact avec son `responsable`. Si la candidature est acceptée, un suivi régulier sera assuré jusqu'à l'accomplissement de l'AHE de sorte à pouvoir quantifier le travail réalisé par l'étudiant.

Arrivé au terme du semestre, le responsable a la charge de vérifier si les critères de réussites ont été atteints et définir la "note" de l'étudiant _(points AHE)_. Le responsable remplit pour se faire une `fiche de notation`.

Celle-ci sera eximanée lors de la prochaine commission AHE afin de vérifier que la validation et le nombre de points attribués sont pertinents en modifiant, si besoin est, le nombre de points ou de la catégorie d'AHE. Cette étape aboutit sur la création d'un `procès verbal de commission` qui sera ensuite transféré à la scolarité.

__[RÉSUMÉ]__ Les étapes sont donc les suivantes :

1. Postulation d'un étudiant sur une AHE en contactant le responsable de cette AHE.
2. Acception de la candidature.
3. Réalisation du travail avec suivi régulier.
4. Arrivé à terme du semestre, remplissage d'une `fiche de notation` individuelle par le responsable de l'AHE.
5. Examen de cette fiche par le comité AHE _(avec d'éventuelles modifications)_.
6. Rédaction d'un `procès verbal de commission`.
7. Transmission du procès verbal à la scolarité.

## II - Objectifs

Il sera donc nécessaire de simplifier les parties suivantes :

- Le dépôt de la fiche de proposition AHE.
- La validation d'une fiche de proposition.
- La consultation et la candidature à une AHE pour l'étudiant.
- La vérification et les modifications éventuelles d'une proposition d'AHE.
- L'établissement du procès verbal de commission.
- Le suivi pour l'étudiant de ses crédits AHE.

## III - Notes diverses

- Pour obtenir `4 crédits ECTS` hors profils, l'étudiant doit valider `100 points AHE` en effectuant des activités dans au moins `2 catégories` différentes.